# README #

This repository stores data files that were too big to be stored on the [Personalization of logical models GitHub repo](https://github.com/sysbio-curie/PROFILE).

You might find here two zipped files:

* METABRIC's data_CNA.txt

* METABRIC's data_expression.txt

* TCGA's data_RNA_Seq_v2_expression_median.txt

This files should be located in their respectives folder in Data folder in [Personalization of logical models GitHub repo](https://github.com/sysbio-curie/PROFILE).